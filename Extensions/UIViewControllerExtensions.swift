//
// Created by mac-184 on 2/2/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentErrorViewWithMessage(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default) {
            _ in })
        self.presentViewController(alert, animated: true) {
        }
    }

    func presentLoadingView() -> UIView {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        let loadingView = UIView(frame: UIScreen.mainScreen().bounds)
        loadingView.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.2)

        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        loadingView.addSubview(loadingIndicator)
        loadingIndicator.center = loadingView.center
        loadingIndicator.startAnimating()

        appDelegate.window?.addSubview(loadingView)

        return loadingView
    }
}