//
// Created by mac-184 on 2/2/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var defaultIdentifier: String {
        return "\(self)"
    }
}