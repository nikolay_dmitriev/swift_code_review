//
// Created by mac-184 on 2/2/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    func stylizeForNotifications() {
        let cornerRadius = 18

        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true

        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 1.0
        self.layer.contentsScale = UIScreen.mainScreen().scale
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 3.0
        self.layer.shadowOffset = CGSizeZero;

        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 18).CGPath
        self.layer.shouldRasterize = true;
        self.layer.shadowColor = UIColor.blackColor().CGColor
    }
}