//
// Created by mac-184 on 2/2/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

extension Array where Element:Equatable {
    mutating func removeObject(object: Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }

    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object)
        }
    }
}