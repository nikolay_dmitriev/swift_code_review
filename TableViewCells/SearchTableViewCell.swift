//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class SearchTableViewCell: UITableViewCell {

//Mark:-Outlets

    @IBOutlet weak var ownerImageView: UIImageView!

    @IBOutlet weak var deviceInformation: UILabel!

    @IBOutlet weak var ownerInformation: UILabel!

//Mark:-Properties

    var device: DeviceEntity?

//Mark:-Public

    func updateWithDevice(device: DeviceEntity) {
        self.device = device

        device.free ? updateAsFreeDevice() : updateAsBusyDevice()
    }

    func updateAsFreeDevice() {
        ownerImageView.image = nil;
        deviceInformation.text = device?.titleAndCompanyId
        ownerInformation.text = "FREE"
    }

    func updateAsBusyDevice() {
        ownerImageView.sd_setImageWithURL(NSURL(string: device?.owner?.url ?? ""), placeholderImage: UIImage(named: "Contact"))
        
        deviceInformation.text = device?.titleAndCompanyId
        ownerInformation.text = device?.owner?.name
    }

}

//Mark:-Extensions

extension SearchTableViewCell {

    func updateWithDeviceForColleagues(device: DeviceEntity) {
        ownerImageView.image = device.owner?.avatar
        deviceInformation.text = device.titleAndCompanyId
        ownerInformation.text = device.owner?.name

        accessoryType = UITableViewCellAccessoryType.None
    }

    func updateWithDeviceForBlackList(device: DeviceEntity) {
        ownerImageView.image = device.temporaryUser?.avatar;
        deviceInformation.text = device.titleAndCompanyId

        guard let timeTaken = device.lastTimeTaken, temporaryUser = device.temporaryUser else {
            return
        }

        let positiveTimeInterval = timeTaken.timeIntervalSinceNow * (-1)
        let intervalInHours = ceil(positiveTimeInterval / 360.0)
        ownerInformation.text = "\(temporaryUser.name) / \(intervalInHours)H"

        accessoryType = UITableViewCellAccessoryType.None
    }

}