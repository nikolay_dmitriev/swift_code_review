//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class MyProfileAssignedDevicesTableViewCell: UITableViewCell {

//Mark:-Properties

    @IBOutlet weak var deviceInformationLabel: UILabel!

    @IBOutlet weak var dueAndReleaseDateLabel: UILabel!

//Mark:-Public

    func updateWithDevice(device: DeviceEntity) {
        deviceInformationLabel.text = device.titleAndCompanyId;

        guard let projectInfo = device.projectInfo else{
            dueAndReleaseDateLabel.text = "No project information available"
            return
        }

        let dueDate = projectInfo.dueDate.stringWithFormatMMMMddYYYY()
        let releaseDate = projectInfo.releaseDate.stringWithFormatMMMMddYYYY()

        dueAndReleaseDateLabel.text = "\(dueDate) - \(releaseDate)"
    }

}
