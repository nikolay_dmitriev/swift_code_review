//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class SignInViewController: UIViewController, UITextFieldDelegate {

//Mark:-Properties

    @IBOutlet weak var usernameTextfield: UITextField! {
        didSet {
            usernameTextfield.delegate = self
        }
    }

    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }

//Mark:-Lifecycle

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = true
    }

//Mark:-Custom Actions

    @IBAction func signInButtonTapped(sender: AnyObject) {
        let username = usernameTextfield.text ?? ""
        let password = passwordTextField.text ?? ""

        let loadingView = self.presentLoadingView()

        DataStorage.sharedInstance.singInUser(username, password: password) {
            guard $0 == nil else{
                loadingView.removeFromSuperview()
                self.presentErrorViewWithMessage($0!)
                return
            }
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.showApplicationMainTabBar()
        }
    }

//Mark:-UITextFieldDelegate

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}