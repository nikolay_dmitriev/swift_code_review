//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import MessageUI
import UIKit

class SearchDetailsViewController: UIViewController {

//Mark:-IBOutlet

    @IBOutlet weak var avatarImage: UIImageView!

    @IBOutlet weak var deviceTitleLabel: UILabel!

    @IBOutlet weak var ownerNameLabel: UILabel!

    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var projectLabel: UILabel!

    @IBOutlet weak var roomLabel: UILabel!

    @IBOutlet weak var phoneNumberButton: UIButton!

    @IBOutlet weak var emailButton: UIButton!

    @IBOutlet weak var requestDeviceButton: UIButton!

//Mark:-Properties

    private var device: DeviceEntity?

    private var isRequestButtonVisible = true

//Mark:-Lifecycle

    convenience init(device: DeviceEntity) {
        self.init()

        self.device = device
    }

    convenience init(deviceRequest: DeviceRequestEntity) {
        self.init()

        isRequestButtonVisible = false
        self.device = deviceRequest.device
    }

//Mark:-Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        updateUi()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.translucent = false
        navigationController?.navigationBarHidden = false
    }


    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        navigationController?.navigationBar.translucent = true
        navigationController?.navigationBarHidden = true
    }

    override func loadView() {
        NSBundle.mainBundle().loadNibNamed("SearchDetails", owner: self, options: nil)
    }

//Mark:-Actions

    @IBAction func phoneButtonTapped(sender: AnyObject) {
        let button = sender as! UIButton
        makeCallToNumber(button.titleLabel!.text!)
    }

    @IBAction func emailButtonTapped(sender: AnyObject) {
        let button = sender as! UIButton

        guard button.titleLabel?.text != nil else{
            print("No email")
            return;
        }

        sendEmailToAddress(button.titleLabel!.text!)
    }

    @IBAction func requestButtontTapped(sender: AnyObject) {
        guard let device = self.device where device.owner != DataStorage.sharedInstance.currentUser else{
            self.presentErrorViewWithMessage("Can't send request")
            return
        }
        disableSendDeviceRequestButton()
        DeviceRequestEntity(device: device, sender: DataStorage.sharedInstance.currentUser, status: DeviceRequestStatus.Pending).save()
    }

//Mark:-Private

    func updateUi() {
        guard let device = device else {
            return
        }

        avatarImage.sd_setImageWithURL(NSURL(string: device.owner?.url ?? ""), placeholderImage: UIImage(named: "Contact"))
        
        deviceTitleLabel.text = device.deviceTitle

        ownerNameLabel.text = "Owner \(device.owner!.name)"

        dateLabel.text = "From \(device.projectInfo!.dueDate.stringWithFormatMMMMddYYYY()) to \(device.projectInfo!.releaseDate.stringWithFormatMMMMddYYYY())"

        projectLabel.text = device.projectInfo?.projectName
        roomLabel.text = device.owner?.room

        phoneNumberButton.titleLabel?.text = device.owner?.phoneNumber
        emailButton.titleLabel?.text = device.owner?.emailAddress

        requestDeviceButton.alpha = isRequestButtonVisible ? 1 : 0

        //Disable button, if current user has pending request for current device
        DataStorage.sharedInstance.deviceRequestsSentByCurrentUser.filter() {
            $0.device.uniqueId == self.device?.uniqueId
        }.first != nil ? disableSendDeviceRequestButton() : ()
    }

    func disableSendDeviceRequestButton() {
        requestDeviceButton.setTitle("Request sent", forState: UIControlState.Normal)
        requestDeviceButton.backgroundColor = UIColor.darkGrayColor()
        requestDeviceButton.enabled = false
    }

    func makeCallToNumber(number: String) {
        let phone = String(format: "tel://%@", number)
        let url = NSURL(string: phone)

        if let realUrl = url {
            UIApplication.sharedApplication().openURL(realUrl)
        }
    }

    func sendEmailToAddress(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self

            mail.setToRecipients([email])

            mail.setMessageBody(emailBody(), isHTML: true)

            presentViewController(mail, animated: true, completion: nil)
        } else {
            print("Can't send email")
        }
    }

    func emailBody() -> String {
        let greetingMsg = String(format: "Hi, %@ !", device!.owner!.name)
        let messageBody = String(format: "Can i take %@ %@ ?", device!.deviceTitle, device!.companyId)
        let userName = DataStorage.sharedInstance.currentUser.name;
        let applicationName = NSBundle.mainBundle().infoDictionary!["CFBundleName"] as! String

        var body = String(format: "<h1>%@</h1>\n", greetingMsg);
        body += String(format: "<p>%@</p>\n", messageBody);
        body += String(format: "<p>Best,</p>\n", messageBody);
        body += String(format: "<p>%@</p>\n", userName);
        body += String(format: "<p>Generated with : %@</p>\n", applicationName);

        return body
    }
}

extension NSDate {
    func stringWithFormatMMMMddYYYY() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, YYYY"
        return dateFormatter.stringFromDate(self)
    }
}

extension SearchDetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        print("finished")
    }
}