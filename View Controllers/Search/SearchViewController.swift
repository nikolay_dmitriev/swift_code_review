//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class SearchViewController: UIViewController {

//Mark:-Outlets

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self;
            tableView.dataSource = self;
        }
    }

    @IBOutlet weak var segmentedControl: UISegmentedControl!

//Mark:-Properties

    private var searchController: UISearchController?

    private var devicesFilteredBySearchCached: [DeviceEntity] = []

    private lazy var allDevices: [DeviceEntity] = self.getAllDevices()
    private lazy var freeDevices: [DeviceEntity] = self.getFreeDevices()

//Mark:-Properties Initialization

    func getAllDevices() -> [DeviceEntity] {
        var devicesWithOwner = DataStorage.sharedInstance.devicesWithOwner
        devicesWithOwner.sortInPlace() {
            return $0.deviceTitle.localizedCaseInsensitiveCompare($1.deviceTitle) == NSComparisonResult.OrderedAscending
        }

        let freeDevices = getFreeDevices()
        let result = freeDevices + devicesWithOwner

        return result
    }

    func getFreeDevices() -> [DeviceEntity] {
        var devices = DataStorage.sharedInstance.freeDevices
        devices.sortInPlace() {
            return $0.deviceTitle.localizedCaseInsensitiveCompare($1.deviceTitle) == NSComparisonResult.OrderedAscending
        }
        return devices
    }

//Mark:-Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        createSearchController();
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBarHidden = true

        DataStorage.sharedInstance.subscribeOnChanges("\(self)") {
            self.updateTableView()
        }

        updateTableView()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        DataStorage.sharedInstance.unsubscribeObserver("\(self)")
    }

//Mark:-Actions

    @IBAction func segmentedControlValueChanged(sender: AnyObject) {
        tableView.reloadData()
    }

//Mark:-Private
    private func updateTableView() {
        self.allDevices = self.getAllDevices()
        self.freeDevices = self.getFreeDevices()
        self.tableView.reloadData()
    }

    private func createSearchController() {
        guard self.searchController == nil else{
            return
        }

        let searchController = UISearchController(searchResultsController: nil)

        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false

        searchController.loadView()
        self.searchController = searchController

        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }

//Mark:-Constants

    private struct constants {
        static let storyboardCellId = "SearchTableViewCell"
        static let cellXib = "SearchTableViewCell"
        static let cellHeight = 55
    }

}

//Mark:-Extensions

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {

        guard let searchPattern = searchController.searchBar.text where searchPattern.characters.count != 0 else {
            //if search was cancelled, reload data
            tableView.reloadData()
            return;
        }

        devicesFilteredBySearchCached = devicesWithoutFilter.filter {
            $0.deviceTitle.lowercaseString.containsString(searchPattern.lowercaseString)
        }

        tableView.reloadData()
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedDevice = devices[indexPath.row]

        guard selectedDevice.free != true else {
            return;
        }

        navigationController?.pushViewController(SearchDetailsViewController(device: selectedDevice), animated: true);
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(constants.cellHeight)
    }

}

extension SearchViewController: UITableViewDataSource {

    var devicesWithoutFilter: [DeviceEntity] {
        get {
            return segmentedControl.selectedSegmentIndex == 0 ? allDevices : freeDevices
        }
    }

    var devices: [DeviceEntity] {
        get {
            if searchController!.active && searchController!.searchBar.text != "" {
                return devicesFilteredBySearchCached
            }
            return devicesWithoutFilter
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(constants.storyboardCellId) as! SearchTableViewCell

        let device = devices[indexPath.row]
        cell.updateWithDevice(device)

        return cell
    }

}