//
// Created by mac-184 on 1/21/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class NotificationsViewController: UIViewController {

//Mark:-Properties

    lazy var viewModel: NotificationsViewModel = {
        return NotificationsViewModel(collectionView: self.collectionView)
    }()

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            viewModel.contactWithDeviceOwnerTapped = presentDetailSearchView
        }
    }

//Mark:-Lifecycle

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBarHidden = true

        DataStorage.sharedInstance.subscribeOnChanges("\(self)") {
            self.viewModel.reload(self.collectionView)
        }
        self.viewModel.reload(self.collectionView)
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        DataStorage.sharedInstance.unsubscribeObserver("\(self)")
    }

//Mark:-Callbacks

    func presentDetailSearchView(deviceRequest: DeviceRequestEntity) {
        navigationController?.pushViewController(SearchDetailsViewController(deviceRequest: deviceRequest), animated: true)
    }
}

extension NotificationsViewController: UICollectionViewDelegate {

}

extension NotificationsViewController: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.collectionView(collectionView, numberOfItemsInSection: section)
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = viewModel.collectionView(collectionView, cellForItemAtIndexPath: indexPath)

        cell.stylizeForNotifications()

        return cell
    }
}

extension NotificationsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var size = collectionView.frame.size

        size.height = 160
        size.width *= 0.99

        return size
    }
}