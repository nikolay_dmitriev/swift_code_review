//
// Created by mac-184 on 1/21/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class NotificationsViewModel: NSObject {

//Marl:-Init

    init(collectionView: UICollectionView) {
        collectionView.registerNib(UINib(nibName: constants.requestXibName, bundle: nil), forCellWithReuseIdentifier: RequestCollectionViewCell.defaultIdentifier)
        collectionView.registerNib(UINib(nibName: constants.reminderXibName, bundle: nil), forCellWithReuseIdentifier: ReminderCollectionViewCell.defaultIdentifier)
    }

//Mark:-Properties

    private lazy var reminders: [DeviceEntity] = self.getExpiredDevices()

    private lazy var requests: [DeviceRequestEntity] = self.getDeviceRequests()

//Mark:-Public

    var contactWithDeviceOwnerTapped: ((deviceRequest:DeviceRequestEntity) -> ())?

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationsNumber()
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        return indexPath.row >= reminders.count ? requestCellForCollectionView(collectionView, cellForItemAtIndexPath: indexPath) :
                reminderCellForCollectionView(collectionView, cellForItemAtIndexPath: indexPath)
    }

    func reload(collectionView: UICollectionView) {
        reminders = getExpiredDevices()
        requests = getDeviceRequests()

        collectionView.reloadData()
    }

//Mark:-Private Property Initializaiton

    func getExpiredDevices() -> [DeviceEntity] {
        return DataStorage.sharedInstance.expiredDevices
    }

    func getDeviceRequests() -> [DeviceRequestEntity] {
        return DataStorage.sharedInstance.notificationsForDeviceRequestsForCurrentUser
    }

//Mark:-Private Cell Creation

    func notificationsNumber() -> Int {
        return reminders.count + requests.count
    }

    func reminderCellForCollectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ReminderCollectionViewCell.defaultIdentifier, forIndexPath: indexPath) as! ReminderCollectionViewCell

        let device = reminders[indexPath.row]

        cell.updateWithDevice(device)

        cell.okButton.addTarget(self, action: "reminderButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.okButton.tag = indexPath.row

        return cell
    }

    func requestCellForCollectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(RequestCollectionViewCell.defaultIdentifier, forIndexPath: indexPath) as! RequestCollectionViewCell

        let correctedIndex = indexPath.row - reminders.count

        let request = requests[correctedIndex]
        cell.updateWithRequest(request)

        cell.acceptOrContactButton.addTarget(self, action: "requestAcceptOrContactButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.acceptOrContactButton.tag = indexPath.row

        cell.ignoreButton.addTarget(self, action: "requestIgnoreButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.ignoreButton.tag = indexPath.row

        return cell
    }

//Mark:-Cells Actions

    func reminderButtonAction(sender: UIButton) {
        let device = reminders[sender.tag]
        device.markAsFree()
        device.save()
    }

    func requestAcceptOrContactButtonAction(sender: UIButton) {
        let correctedIndex = sender.tag - reminders.count
        let request = requests[correctedIndex]
        request.status == DeviceRequestStatus.Pending ? markRequestAsAccepted(request) : contactWithDeviceOwner(request)
    }

    func requestIgnoreButtonAction(sender: UIButton) {
        let correctedIndex = sender.tag - reminders.count
        let request = requests[correctedIndex]
        request.delete()
    }

//Mark:-Actions based on request status

    func markRequestAsAccepted(request: DeviceRequestEntity) {
        request.status = DeviceRequestStatus.Accepted
        request.save()
    }

    func contactWithDeviceOwner(request: DeviceRequestEntity) {
        contactWithDeviceOwnerTapped?(deviceRequest: request)
    }

//Mark:-Constants

    struct constants {
        static let reminderXibName = "ReminderViewCell"
        static let requestXibName = "RequestViewCell"
    }

}