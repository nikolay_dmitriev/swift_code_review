//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class SignUpViewController: UIViewController, UITextFieldDelegate {

//Mark:-Properties

    @IBOutlet weak var usernameTextField: UITextField! {
        didSet {
            usernameTextField.delegate = self
        }
    }

    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.delegate = self
        }
    }

    @IBOutlet weak var phoneNumberTextField: UITextField! {
        didSet {
            phoneNumberTextField.delegate = self
        }
    }

    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.delegate = self
        }
    }

    @IBOutlet weak var roomTextField: UITextField! {
        didSet {
            roomTextField.delegate = self
        }
    }

//Mark:-Lifecycle

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = false
    }

//Mark:-Custom Actions

    @IBAction func signUpButtonTapped(sender: AnyObject) {
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let phoneNumber = phoneNumberTextField.text ?? ""
        let room = roomTextField.text ?? ""
        let email = emailTextField.text ?? ""

        //temp user, just to send information to DataStorage, not real user instance
        let userInformation = UserEntity(ownerName: username, avatar: nil, room: room, phoneNumber: phoneNumber, emailAddress: email, uniqueId: "")

        let loadingView = self.presentLoadingView()

        DataStorage.sharedInstance.signUpUser(username, password: password, userInfromation: userInformation) {
            guard $0 == nil else{
                loadingView.removeFromSuperview()
                self.presentErrorViewWithMessage($0!)
                return
            }
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.showApplicationMainTabBar()
        }
    }

//Mark:-UITextFieldDelegate

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
