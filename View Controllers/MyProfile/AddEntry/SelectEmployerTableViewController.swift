//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class SelectEmployerTableViewController: UIViewController {

//Mark-Properties

    var sumbitSelectedEmployer: ((selectedEmployer:UserEntity) -> ())?

//Mark:-Init

    convenience init(sumbitSelectedDevice: ((selectedDevice:UserEntity) -> ())) {
        self.init()

        self.sumbitSelectedEmployer = sumbitSelectedDevice
    }

//Mark:-Lazy Properties

    private lazy var table: UITableView = {
        let tableView = UITableView()

        tableView.delegate = self
        tableView.dataSource = self

        tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: constants.ownerCellId)

        self.view.addSubview(tableView)

        tableView.center = self.view.center
        tableView.frame = self.view.frame

        return tableView
    }()

    private lazy var employers: [UserEntity] = {
        var users = DataStorage.sharedInstance.companyUsers;
        users.removeObject(DataStorage.sharedInstance.currentUser)
        return users
    }()

//Mark:-Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.table.reloadData()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBarHidden = false
    }
}

//Mark:-Extensions

extension SelectEmployerTableViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(constants.ownerCellId)!

        let employer = employers[indexPath.row]

        cell.textLabel?.text = employer.name
        cell.imageView?.image = employer.avatar

        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employers.count
    }

//Mark:-Constants

    private struct constants {
        static let ownerCellId = "SelectEmployerTableViewCell"
    }

}

extension SelectEmployerTableViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if let sumbitSelectedEmployer = sumbitSelectedEmployer {
            let selectedEmployer = employers[indexPath.row]
            sumbitSelectedEmployer(selectedEmployer: selectedEmployer)
        }

        navigationController?.popViewControllerAnimated(true)
    }

}