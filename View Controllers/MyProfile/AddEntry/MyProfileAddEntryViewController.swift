//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class MyProfileAddEntryViewController: UIViewController {

//Mark:-Outlets

    @IBOutlet weak var saturateBackground: UIView!

    @IBOutlet weak var employerNameLabel: UILabel!
    @IBOutlet weak var selectDeviceLabel: UILabel!

    @IBOutlet weak var employerPhoto: UIImageView!

    var deviceToBorrow: DeviceEntity?
    var deviceTemporaryUser: UserEntity?

//Mark:-Lifecycle

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = true
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animateWithDuration(0.4) {
            self.saturateBackground.alpha = 0.5
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        self.saturateBackground.alpha = 0
    }

    override func loadView() {
        NSBundle.mainBundle().loadNibNamed("MyProfileAddEntry", owner: self, options: nil)
    }

//Mark:-Actions

    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func doneButtonTapped(sender: AnyObject) {
        guard let newUser = deviceTemporaryUser, device = deviceToBorrow else{
            self.presentErrorViewWithMessage("Please fill all fields")
            return
        }

        guard newUser.uniqueId != DataStorage.sharedInstance.currentUser.uniqueId else{
            self.presentErrorViewWithMessage("You can't borrow device to yourself")
            return
        }

        guard device.temporaryUser == nil else{
            self.presentErrorViewWithMessage("Can't borrow device. Device already in use by \(device.temporaryUser!.name)")
            return
        }

        device.lastTimeTaken = NSDate()
        device.temporaryUser = newUser

        device.save()

        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func selectDeviceTapped(sender: AnyObject) {
        let controller = SelectDeviceTableViewController() {
            (selectedDevice: DeviceEntity) in
            self.selectDeviceLabel.text = selectedDevice.titleAndCompanyId
            self.deviceToBorrow = selectedDevice
        }

        navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func selectEmployerTapped(sender: AnyObject) {
        let controller = SelectEmployerTableViewController() {
            (selectedEmployer: UserEntity) in
            self.employerNameLabel.text = selectedEmployer.name
            self.employerPhoto.image = selectedEmployer.avatar
            self.deviceTemporaryUser = selectedEmployer
        }

        navigationController?.pushViewController(controller, animated: true)
    }

}