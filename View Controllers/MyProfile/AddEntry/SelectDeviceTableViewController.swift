//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class SelectDeviceTableViewController: UIViewController {

//Mark-Properties

    var sumbitSelectedDevice: ((selectedDevice:DeviceEntity) -> ())?

//Mark:-Init

    convenience init(sumbitSelectedDevice: ((selectedDevice:DeviceEntity) -> ())) {
        self.init()

        self.sumbitSelectedDevice = sumbitSelectedDevice
    }

//Mark:-Lazy Properties

    private lazy var table: UITableView = {
        let tableView = UITableView()

        tableView.delegate = self
        tableView.dataSource = self

        tableView.registerNib(UINib(nibName: constants.ownerCellXib, bundle: nil), forCellReuseIdentifier: constants.ownerCellId)

        self.view.addSubview(tableView)

        tableView.center = self.view.center
        tableView.frame = self.view.frame

        return tableView
    }()

    private lazy var devicesAssignedToCurrentUser: [DeviceEntity] = {

        var devices = DataStorage.sharedInstance.devicesAssignedToCurrentUser

        devices = devices.filter() {
            return $0.temporaryUser == nil
        }

        return devices
    }()


//Mark:-Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.table.reloadData()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBarHidden = false
    }

}

//Mark:-Extensions

extension SelectDeviceTableViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(constants.ownerCellId) as! MyProfileAssignedDevicesTableViewCell

        let device = devicesAssignedToCurrentUser[indexPath.row]
        cell.updateWithDevice(device)

        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devicesAssignedToCurrentUser.count
    }

//Mark:-Constants

    private struct constants {
        static let ownerCellId = "MyProfileViewModelProtocol"
        static let ownerCellXib = "MyProfileAssignedDevicesTableViewCell"
    }

}

extension SelectDeviceTableViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if let sumbitSelectedDevice = sumbitSelectedDevice {
            let selectedDevice = devicesAssignedToCurrentUser[indexPath.row]
            sumbitSelectedDevice(selectedDevice: selectedDevice)
        }

        navigationController?.popViewControllerAnimated(true)
    }

}