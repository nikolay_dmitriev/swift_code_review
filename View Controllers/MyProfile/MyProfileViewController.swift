//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class MyProfileViewController: UIViewController {

//Mark:-Outlets

    @IBOutlet weak var addEntryAboutDeviceTakenButton: UIButton!

    @IBOutlet weak var ownedDarkListSegmentedControl: UISegmentedControl!

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }

//Mark:-Properties

    private lazy var ownedDevicesViewModel: MyProfileOwnedDevicesViewModel = {
        return MyProfileOwnedDevicesViewModel(tableView: self.tableView)
    }()

    private lazy var blackListDevicesViewModel: MyProfileBlackListDevicesViewModel = {
        return MyProfileBlackListDevicesViewModel(tableView: self.tableView)
    }()

    private lazy var logoutButton: UIButton = {
        let button = UIButton()
        button.setTitle("Logout", forState: UIControlState.Normal)
        button.backgroundColor = UIColor.redColor()
        button.addTarget(self, action: "logoutButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        return button
    }()

//Mark:-Computed Properties

    var viewModel: MyProfileViewModelProtocol {
        return ownedDarkListSegmentedControl.selectedSegmentIndex == 0 ? ownedDevicesViewModel : blackListDevicesViewModel
    }

//Mark:-Lifecycle

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        DataStorage.sharedInstance.subscribeOnChanges("\(self)") {
            self.viewModel.reloadData(self.tableView)
        }

        viewModel.reloadData(self.tableView)
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        DataStorage.sharedInstance.unsubscribeObserver("\(self)")
    }

//Mark:-Custom Actions

    @IBAction func segmentedControlChanged(sender: AnyObject) {
        let segmentedControl = sender as! UISegmentedControl
        addEntryAboutDeviceTakenButton.alpha = segmentedControl.selectedSegmentIndex == 0 ? 0 : 1

        viewModel.reloadData(tableView)
    }

    @IBAction func addEntryButtonTapped(sender: AnyObject) {
        let addEntryView = MyProfileAddEntryViewController()
        let addEntryNavigationController = UINavigationController(rootViewController: addEntryView)

        addEntryNavigationController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext

        let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        let viewController = appDelegate?.window?.rootViewController

        viewController?.presentViewController(addEntryNavigationController, animated: true, completion: nil)
    }

    func logoutButtonTapped(sender: UIButton) {
        DataStorage.sharedInstance.logoutUser()
    }

//Mark:-Constants

    private struct constants {
        static let cellHeight = 55
        static let footerHeight = 44
    }

}

//Mark:-Extensions

extension MyProfileViewController: UITableViewDelegate {

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(constants.cellHeight)
    }

    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if ownedDarkListSegmentedControl.selectedSegmentIndex == 0 && section == 1 {
            return logoutButton
        }

        if ownedDarkListSegmentedControl.selectedSegmentIndex == 1 {
            return logoutButton
        }
        return nil
    }

    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if ownedDarkListSegmentedControl.selectedSegmentIndex == 0 && section == 1 {
            return CGFloat(constants.footerHeight)
        }

        if ownedDarkListSegmentedControl.selectedSegmentIndex == 1 {
            return CGFloat(constants.footerHeight)
        }
        return CGFloat(0)
    }
}

extension MyProfileViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return viewModel.tableView(tableView, cellForRowAtIndexPath: indexPath)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.tableView(tableView, titleForHeaderInSection: section)
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return viewModel.numberOfSectionsInTableView(tableView)
    }

}

//Mark:-Protocols

protocol MyProfileViewModelProtocol {

    func numberOfSectionsInTableView(tableView: UITableView) -> Int

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?

    func reloadData(tableView: UITableView)

}