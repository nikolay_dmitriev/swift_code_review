//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class MyProfileBlackListDevicesViewModel: MyProfileViewModelProtocol {

//Mark:-Properties

    private lazy var deviceBorrowedFromCurrentUser: [DeviceEntity] = self.findDevicesBorrowedByUser()

//Mark:-Init

    init(tableView: UITableView) {
        tableView.registerNib(UINib(nibName: constants.searchCellXib, bundle: nil), forCellReuseIdentifier: SearchTableViewCell.defaultIdentifier)
    }

//Mark:-MyProfileViewModelProtocol

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let device = deviceBorrowedFromCurrentUser[indexPath.row]

        let cell = tableView.dequeueReusableCellWithIdentifier(SearchTableViewCell.defaultIdentifier) as! SearchTableViewCell
        cell.updateWithDeviceForBlackList(device)

        return cell
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceBorrowedFromCurrentUser.count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "You devices in other hands"
    }

    func reloadData(tableView: UITableView) {
        self.deviceBorrowedFromCurrentUser = findDevicesBorrowedByUser()
        tableView.reloadData()
    }

//Mark:-Private

    private func findDevicesBorrowedByUser() -> [DeviceEntity] {
        var devices = DataStorage.sharedInstance.companyDevices.filter {
            guard let _ = $0.temporaryUser, owner = $0.owner else{
                return false
            }
            return owner.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
        }

        devices.sortInPlace() {
            let firstTimeInterval = $0.0.lastTimeTaken?.timeIntervalSinceNow
            let secondTimeInterval = $0.1.lastTimeTaken?.timeIntervalSinceNow
            return firstTimeInterval > secondTimeInterval
        }

        return devices
    }

//Mark:-Constants

    private struct constants {
        static let searchCellXib = "SearchTableViewCell"
    }

}