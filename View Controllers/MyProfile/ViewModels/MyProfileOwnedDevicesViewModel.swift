//
// Created by mac-184 on 1/20/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class MyProfileOwnedDevicesViewModel: MyProfileViewModelProtocol {

//Mark:-Properties

    private lazy var assignToUserDevices: [DeviceEntity] = self.getAssignToUserDevices()

    private lazy var colleaguesDevices: [DeviceEntity] = self.getColleaguesDevices()

//Mark:-Properties Initialization

    func getAssignToUserDevices() -> [DeviceEntity] {
        return DataStorage.sharedInstance.devicesAssignedToCurrentUser
    }

    func getColleaguesDevices() -> [DeviceEntity] {
        return DataStorage.sharedInstance.colleaguesDevicesForCurrentUser
    }

//Mark:-Init

    init(tableView: UITableView) {
        tableView.registerNib(UINib(nibName: constants.ownerCellXib, bundle: nil), forCellReuseIdentifier: MyProfileAssignedDevicesTableViewCell.defaultIdentifier)
        tableView.registerNib(UINib(nibName: constants.colleaguesCellXib, bundle: nil), forCellReuseIdentifier: SearchTableViewCell.defaultIdentifier)
    }

//Mark:-MyProfileViewModelProtocol

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let device = deviceListForSectionWithIndex(indexPath.section)[indexPath.row]

        var resultCell: UITableViewCell?

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(MyProfileAssignedDevicesTableViewCell.defaultIdentifier) as! MyProfileAssignedDevicesTableViewCell
            cell.updateWithDevice(device)
            resultCell = cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(SearchTableViewCell.defaultIdentifier) as! SearchTableViewCell
            cell.updateWithDeviceForColleagues(device)
            resultCell = cell
        }

        return resultCell!
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceListForSectionWithIndex(section).count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeaderForIndex(section)
    }

    func reloadData(tableView: UITableView) {
        assignToUserDevices = getAssignToUserDevices()
        colleaguesDevices = getColleaguesDevices()

        tableView.reloadData()
    }

//Mark:-Private

    func deviceListForSectionWithIndex(index: Int) -> [DeviceEntity] {
        return index == 0 ? assignToUserDevices : colleaguesDevices
    }

    func sectionHeaderForIndex(index: Int) -> String {
        return index == 0 ? "Assigned to you" : "Colleagues devices"
    }

//Mark:-Constants

    private struct constants {
        static let ownerCellXib = "MyProfileAssignedDevicesTableViewCell"

        static let colleaguesCellXib = "SearchTableViewCell"
    }

}
