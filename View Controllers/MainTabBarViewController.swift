//
// Created by mac-184 on 1/21/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit
import Parse

class MainTabBarHandler {

//Mark:-Init

    static let sharedInstance = MainTabBarHandler()

//Mark:-Properties

    lazy var tabBarController: UITabBarController = self.createTabBar()

    private var notificationBadge: UITabBarItem?

//Mark:-Private

    private func createTabBar() -> UITabBarController {
        let resultTabBar = UITabBarController()

        let searchController = SearchViewController()
        let searchNavigationController = UINavigationController(rootViewController: searchController)
        let searchIcon = UITabBarItem(title: "Search", image: UIImage(named: "TabBarSearch"), selectedImage: UIImage(named: "TabBarSearch"))
        searchNavigationController.tabBarItem = searchIcon

        let myProfileController = MyProfileViewController()
        let myProfileNavigationController = UINavigationController(rootViewController: myProfileController)
        let myProfileIcon = UITabBarItem(title: "My Profile", image: UIImage(named: "TabBarProfile"), selectedImage: UIImage(named: "TabBarProfile"))
        myProfileNavigationController.tabBarItem = myProfileIcon

        let notificationController = NotificationsViewController()
        let notificationsNavigationController = UINavigationController(rootViewController: notificationController)
        let notificationIcon = UITabBarItem(title: "Notifications", image: UIImage(named: "TabBarNotifications"), selectedImage: UIImage(named: "TabBarNotifications"))

        self.notificationBadge = notificationIcon

        notificationsNavigationController.tabBarItem = notificationIcon

        let controllers = [searchNavigationController, myProfileNavigationController, notificationsNavigationController]

        resultTabBar.viewControllers = controllers;
        resultTabBar.tabBar.translucent = false

        DataStorage.sharedInstance.subscribeOnChanges("gg", observer: updateTableViewBadge)
        updateTableViewBadge()

        return resultTabBar
    }

    private func updateTableViewBadge() {
        let result = DataStorage.sharedInstance.notificationsForDeviceRequestsForCurrentUser.count + DataStorage.sharedInstance.expiredDevices.count
        self.notificationBadge?.badgeValue = result == 0 ? nil : "\(result)"
    }
}