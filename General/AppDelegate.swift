//
//  AppDelegate.swift
//  CompanyDevices
//
//  Created by mac-184 on 1/19/16.
//  Copyright © 2016 NikolayDmitriev. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {


        //    DeviceParseEntity.registerSubclass()

        Parse.setApplicationId("vDdlIs69KFD10NG1qlGRIfhgzTM7dKRjKp9y8O5q",
                clientKey: "z7nk5MF3VAtmbDVQ9wJ4YSmYEFwivaEjDGPoBWKM")

        DataStorage.sharedInstance.canSkipAuthorization ? showApplicationMainTabBar() : showLogin()

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//Mark:-Public

    func showLogin() {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = UIStoryboard(name: constants.storyboardName, bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier(constants.loginStoryboardId)
        DataStorage.sharedInstance.fetchData()
        window!.makeKeyAndVisible()
    }

    func showApplicationMainTabBar() {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.rootViewController = UIStoryboard(name: constants.storyboardName, bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier(constants.mainTabBarStoryboardId)
        window!.makeKeyAndVisible()
        subscribeOnNotificationsToUpdateTabBarBadge()
    }

//Mark:-Private

    private func subscribeOnNotificationsToUpdateTabBarBadge() {
        DataStorage.sharedInstance.fetchData()
        DataStorage.sharedInstance.subscribeOnChanges("\(self)") {
            [weak self] in
            if let actualSelf = self {
                let tabBar = actualSelf.window?.rootViewController as? UITabBarController
                let controllers = tabBar?.viewControllers

                let totalNotificationNumber = DataStorage.sharedInstance.expiredDevices.count + DataStorage.sharedInstance.notificationsForDeviceRequestsForCurrentUser.count
                controllers?.last?.tabBarItem.badgeValue = String(totalNotificationNumber)
            }
        }
    }

//Mark:-Constants

    private struct constants {
        static let storyboardName = "Main"
        static let loginStoryboardId = "LoginStoryboardId"
        static let mainTabBarStoryboardId = "MainTabBarStoryboardId"
    }


}