//
// Created by mac-184 on 1/21/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class ReminderCollectionViewCell: UICollectionViewCell {

//Mark:-Properties

    @IBOutlet weak var deviceTitleLabel: UILabel!

    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var okButton: UIButton!

//Mark:-Public

    func updateWithDevice(device: DeviceEntity) {
        deviceTitleLabel.text = device.titleAndCompanyId
        descriptionLabel.text = "\(device.deviceTitle) is expired. Update due date or release the device if you don't need it anymore"
    }

}