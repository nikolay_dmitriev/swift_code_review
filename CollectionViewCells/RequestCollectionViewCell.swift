//
// Created by mac-184 on 1/21/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class RequestCollectionViewCell: UICollectionViewCell {

//Mark:-Outlets

    @IBOutlet weak var acceptOrContactButton: UIButton!
    @IBOutlet weak var ignoreButton: UIButton!

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var deviceLabel: UILabel!
    @IBOutlet weak var requestTitleLabel: UILabel!

    @IBOutlet weak var photoImage: UIImageView!

//Mark:-Properties

    private var request: DeviceRequestEntity?

//Mark:-Public

    func updateWithRequest(request: DeviceRequestEntity) {
        self.request = request
        updateUi()
    }

//Mark:-Private

    private func updateUi() {
        request?.status == DeviceRequestStatus.Pending ? updateAsPendingRequest() : updateAsAcceptedRequest()
    }

    private func updateAsPendingRequest() {
        acceptOrContactButton.setTitle("Accept Request", forState: UIControlState.Normal)

        let employer = request?.sender.name ?? "Someone"
        let deviceTitle = request?.device.titleAndCompanyId ?? "Some Device"
        infoLabel.text = "\(employer) is looking for \(deviceTitle). Would you like to help ?"

        deviceLabel.text = request?.device.titleAndCompanyId

        requestTitleLabel.text = "DEVICE REQUEST"
        requestTitleLabel.textColor = UIColor.orangeColor()

        photoImage.sd_setImageWithURL(NSURL(string: request?.sender.url ?? ""), placeholderImage: UIImage(named: "Contact"))
    }

    private func updateAsAcceptedRequest() {
        guard let owner = request?.device.owner else {
            return
        }

        acceptOrContactButton.setTitle("Contact \(owner.name)", forState: UIControlState.Normal)

        let room = owner.room ?? "Unknown"
        let deviceTitle = request?.device.titleAndCompanyId ?? "Some Device"
        infoLabel.text = "\(owner.name) (Room-\(room)) is ready to provide \(deviceTitle)"

        deviceLabel.text = request?.device.titleAndCompanyId

        requestTitleLabel.text = "ACCEPTED REQUEST"
        requestTitleLabel.textColor = UIColor.greenColor()

        photoImage.sd_setImageWithURL(NSURL(string: request?.device.owner?.url ?? ""), placeholderImage: UIImage(named: "Contact"))
    }

}
