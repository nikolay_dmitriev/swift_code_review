//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

enum DeviceRequestStatus: Int {
    case Pending = 1
    case Accepted = 2
    case Ignored = 3
}

class DeviceRequestEntity: NSObject {

//Mark:-Properties

    var uniqueId: String?

    let sender: UserEntity
    let device: DeviceEntity
    var status: DeviceRequestStatus

//Mark:-Init

    init(device: DeviceEntity, sender: UserEntity, status: DeviceRequestStatus) {
        self.sender = sender
        self.device = device
        self.status = status
    }

}

extension DeviceRequestEntity {

    func save() {
        DataStorage.sharedInstance.saveRequest(self)
    }

    func delete() {
        DataStorage.sharedInstance.deleteRequest(self)
    }

}