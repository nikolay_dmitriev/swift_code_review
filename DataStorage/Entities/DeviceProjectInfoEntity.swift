//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

class DeviceProjectInfoEntity {

//Mark:-Properties

    var dueDate: NSDate
    var releaseDate: NSDate
    var projectName: String?

//Mark:-Init

    init(dueDate: NSDate, releaseDate: NSDate, projectName: String?) {
        self.dueDate = dueDate;
        self.releaseDate = releaseDate;
        self.projectName = projectName;
    }

}
