//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class UserEntity: NSObject {

//Mark:-Properties

    let name: String

    let room: String?
    let phoneNumber: String?
    let emailAddress: String?

    var photo: UIImage?
    var url = ""    //Added for SdWebImage test

    let uniqueId: String

//Mark:-Computed Properties

    var avatar: UIImage {
        return photo ?? UIImage(named: constants.noPhotoAvatarImageName)!
    }

//Mark:-Init

    init(ownerName: String, avatar: UIImage?, room: String?, phoneNumber: String?, emailAddress: String?, uniqueId: String) {
        self.name = ownerName
        self.photo = avatar
        self.room = room
        self.phoneNumber = phoneNumber
        self.emailAddress = emailAddress
        self.uniqueId = uniqueId
    }

    convenience init(ownerName: String) {
        self.init(ownerName: ownerName, avatar: nil, room: "", phoneNumber: "", emailAddress: "", uniqueId: "")
    }

//Mark:-Private Constants

    private struct constants {
        static let noPhotoAvatarImageName = "Contact"
    }

}