//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

class DeviceEntity: NSObject {

//Mark:-Properties

    let deviceTitle: String
    let companyId: String

    var owner: UserEntity?
    var projectInfo: DeviceProjectInfoEntity?

    var temporaryUser: UserEntity?
    var lastTimeTaken: NSDate?

    var uniqueId: String?

//Mark:-Computed Properties

    var free: Bool {
        get {
            return owner == nil;
        }
    }

//Mark:-Init

    init(deviceTitle: String, companyId: String, owner: UserEntity?, currentUser: UserEntity?, projectInfo: DeviceProjectInfoEntity?) {
        self.deviceTitle = deviceTitle
        self.owner = owner
        self.companyId = companyId
        self.temporaryUser = currentUser
        self.projectInfo = projectInfo
    }

    convenience init(deviceTitle: String, companyId: String) {
        self.init(deviceTitle: deviceTitle, companyId: companyId, owner: nil, currentUser: nil, projectInfo: nil)
    }

}

//Mark:-Extensions

extension DeviceEntity {

    var titleAndCompanyId: String {
        return String(format: "%@ ( %@ )", self.deviceTitle, self.companyId)
    }

    func save() {
        DataStorage.sharedInstance.saveDevice(self)
    }

    func markAsFree() {
        owner = nil
        projectInfo = nil
        temporaryUser = nil
    }
    
}