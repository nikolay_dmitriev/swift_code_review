//
// Created by mac-184 on 1/19/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class DataStorage {

    static let sharedInstance = DataStorage()

//Mark:-Parse

    private let parseQueue = ParseOperationQueue()

//Mark:-Data

    private(set) var companyDevices: [DeviceEntity] = []

    private(set) var companyUsers: [UserEntity] = []

    private(set) var deviceRequests: [DeviceRequestEntity] = []

    private var registeredObservers: [String:() -> ()] = [:]

//Mark:-Computed Properties

    var currentUser: UserEntity = UserEntity(ownerName: "Offline")

//Mark:-Public

    func subscribeOnChanges(observerKey: String, observer: () -> ()) {
        registeredObservers[observerKey] = observer
    }

    func unsubscribeObserver(observerKey: String) {
        registeredObservers.removeValueForKey(observerKey)
    }

    func fetchData() {
        // parseQueue.beginFetchingFromParse(onFetchComplete)
        parseQueue.beginFetchingFromParse {
            [weak self] (parseUsers, parseDevices, deviceRequests) -> () in
            if let actualSelf = self {
                actualSelf.onFetchComplete(parseUsers, parseDevices: parseDevices, deviceRequests: deviceRequests)
            }
        }
    }

    private func onFetchComplete(parseUsers: [PFUser]?, parseDevices: [DeviceParseEntity]?, deviceRequests: [DeviceRequestParseEntity]?) {
        let converter = ParseToLocalConverter()
        converter.parseUsers = parseUsers

        //To keep encapsulation :)
        converter.addUserEntityToDataStorage = {
            [weak self] in
            if let actualSelf = self {
                actualSelf.companyUsers.append($0)
            }
        }
        converter.convertUsers()

        converter.parseDevices = parseDevices
        converter.addDeviceToDataStorage = {
            [weak self] in
            if let actualSelf = self {
                actualSelf.companyDevices.append($0)
            }
        }
        converter.convertDevices()

        converter.parseRequests = deviceRequests
        converter.replaceRequestsInDataStorage = {
            [weak self] in
            if let actualSelf = self {
                actualSelf.deviceRequests = $0
            }
        }
        converter.convertRequests()

        currentUser = localUserMappingById(PFUser.currentUser()?.objectId ?? "") ?? UserEntity(ownerName: "Errror")

        notifyObservers()
    }

//Mark:-Private

    private func notifyObservers() {
        registeredObservers.forEach {
            $0.1()
        }
    }

}

//Mark:-Extensions

extension DataStorage {

    func saveRequest(request: DeviceRequestEntity) {
        let isOnParse = request.uniqueId != nil
        if !isOnParse {
            parseQueue.uploadDeviceRequestToParse(request)
            deviceRequests.append(request)
            return
        }
        parseQueue.uploadDeviceRequestChangesToParse(request)
        //we upload requests changes only for accepted requests, so, accepted request should be hidden from device owner
        deviceRequests.removeObject(request)
        self.notifyObservers()
    }

    func deleteRequest(request: DeviceRequestEntity) {
        deviceRequests.removeObject(request)
        parseQueue.deleteDeviceRequestFromParse(request)

        self.notifyObservers()
    }

    func saveDevice(device: DeviceEntity) {
        parseQueue.uploadDeviceChangesToParse(device)
        self.notifyObservers()
    }

}

extension DataStorage {

//Mark:-Computed Properties

    var expiredDevices: [DeviceEntity] {
        get {
            let expiredDevices = companyDevices.filter {
                guard let owner = $0.owner, projectInfo = $0.projectInfo else{
                    return false
                }
                let deviceIsAssignedToCurrentUser = owner.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
                let deviceReleaseDateIsExperied = projectInfo.releaseDate.isLessThenDate(NSDate())

                return deviceIsAssignedToCurrentUser && deviceReleaseDateIsExperied
            }
            return expiredDevices
        }
    }

    var devicesAssignedToCurrentUser: [DeviceEntity] {
        get {
            let freeDevices = companyDevices.filter {
                guard let owner = $0.owner else{
                    return false
                }
                return owner.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
            }
            return freeDevices
        }
    }

    var freeDevices: [DeviceEntity] {
        get {
            let freeDevices = companyDevices.filter {
                return $0.free
            }
            return freeDevices
        }
    }

    var devicesWithOwner: [DeviceEntity] {
        get {
            let freeDevices = companyDevices.filter {
                return !$0.free
            }
            return freeDevices
        }
    }

    var colleaguesDevicesForCurrentUser: [DeviceEntity] {
        get {
            let devices = DataStorage.sharedInstance.companyDevices.filter {
                guard let temporaryUser = $0.temporaryUser else{
                    return false
                }
                return temporaryUser.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
            }
            return devices
        }
    }

    var notificationsForDeviceRequestsForCurrentUser: [DeviceRequestEntity] {
        get {
            let requests = DataStorage.sharedInstance.deviceRequests.filter {
                let onlyCurrentUser = $0.sender.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
                let statusIsApproved = $0.status == DeviceRequestStatus.Accepted
                let statusIsPending = $0.status == DeviceRequestStatus.Pending
                //requests which were approved and request sent to current user
                return (onlyCurrentUser && statusIsApproved) || (!onlyCurrentUser && statusIsPending)
            }
            return requests
        }
    }

    var deviceRequestsSentByCurrentUser: [DeviceRequestEntity] {
        get {
            let requests = DataStorage.sharedInstance.deviceRequests.filter {
                let senderIsCurrentUser = $0.sender.uniqueId == DataStorage.sharedInstance.currentUser.uniqueId
                let statusIsPending = $0.status == DeviceRequestStatus.Pending
                return senderIsCurrentUser && statusIsPending
            }
            return requests
        }
    }

//Mark:-Public

    //need for mapping parse objects into local objects, not optimized

    func localUserMappingById(uniqueId: String) -> UserEntity? {
        return companyUsers.filter() {
            return $0.uniqueId == uniqueId
        }.first
    }

    func localDeviceMappingById(uniqueId: String) -> DeviceEntity? {
        return companyDevices.filter() {
            return $0.uniqueId == uniqueId
        }.first
    }

}

//Mark:- Parse Authorizations

extension DataStorage {

    func singInUser(username: String, password: String, callback: (error:String?) -> ()) {
        PFUser.logInWithUsernameInBackground(username, password: password) {
            (user, error) -> Void in
            if let occuredError = error {
                print("\(occuredError)")
                callback(error: "\(occuredError.description)")
                return
            }
            callback(error: nil)
        }
    }

    func signUpUser(username: String, password: String, userInfromation: UserEntity, callback: (error:String?) -> ()) {
        let user = PFUser()

        user.username = username
        user.password = password

        user.room = userInfromation.room ?? ""
        user.phoneNumber = userInfromation.phoneNumber ?? ""
        user.emailAddress = userInfromation.emailAddress ?? ""

        user.signUpInBackgroundWithBlock() {
            (user, error) -> Void in
            if let occuredError = error {
                print("\(occuredError)")
                callback(error: "\(occuredError.description)")
                return
            }
            callback(error: nil)
        }
    }

    func logoutUser() {
        PFUser.logOut()

        registeredObservers = [:]
        currentUser = UserEntity(ownerName: "Offline")
        parseQueue.stopFetching()

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.showLogin()
    }

    var canSkipAuthorization: Bool {
        return PFUser.currentUser() != nil
    }

}