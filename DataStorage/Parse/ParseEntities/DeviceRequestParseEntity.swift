//
// Created by mac-184 on 1/25/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class DeviceRequestParseEntity: PFObject, PFSubclassing {

//Mark:-Parse Related

    override class func initialize() {

        struct Static {
            static var onceToken: dispatch_once_t = 0;
        }

        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }

    }

    static func parseClassName() -> String {
        return "DeviceRequest"
    }

//Mark:-Properties

    @NSManaged var sender: PFUser?
    @NSManaged var device: DeviceParseEntity?
    @NSManaged var currentStatus: Int

    func convertToLocalRequest() -> DeviceRequestEntity {
        let mappedSender = DataStorage.sharedInstance.localUserMappingById(sender?.objectId ?? "")
        let mappedDevice = DataStorage.sharedInstance.localDeviceMappingById(device?.objectId ?? "")
        let mappedStatus = DeviceRequestStatus(rawValue: currentStatus)

        //Request is validated before save, so, they always has all fields
        let deviceRequest = DeviceRequestEntity(device: mappedDevice!, sender: mappedSender!, status: mappedStatus!)
        deviceRequest.uniqueId = objectId
        return deviceRequest
    }

}
