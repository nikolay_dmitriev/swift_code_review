//
// Created by mac-184 on 1/22/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

extension PFUser {

    var room: String {
        get {
            return self.objectForKey("room") as? String ?? ""
        }
        set {
            self.setObject(newValue, forKey: "room")
        }
    }

    var phoneNumber: String {
        get {
            return self.objectForKey("phoneNumber") as? String ?? ""
        }
        set {
            self.setObject(newValue, forKey: "phoneNumber")
        }
    }

    var photo: PFFile? {
        get {
            return self.objectForKey("Photo") as? PFFile
        }
        set {
            self.setObject(newValue!, forKey: "Photo")
        }
    }

    var emailAddress: String {
        get {
            return self.objectForKey("emailAddress") as? String ?? ""
        }
        set {
            self.setObject(newValue, forKey: "emailAddress")
        }
    }

    var uniqueId: String {
        return self.objectId ?? ""
    }

    func convertToUserEntity() -> UserEntity {
        let convertedUser = UserEntity(ownerName: self.username ?? "", avatar: nil, room: self.room, phoneNumber: self.phoneNumber, emailAddress: self.emailAddress, uniqueId: self.uniqueId)
        
        convertedUser.url = self.photo?.url ?? ""

        return convertedUser
    }

}