//
// Created by mac-184 on 1/22/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class DeviceParseEntity: PFObject, PFSubclassing {

//Mark:-Parse Related

    override class func initialize() {

        struct Static {
            static var onceToken: dispatch_once_t = 0;
        }

        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }

    static func parseClassName() -> String {
        return "Device"
    }

//Mark:-Properties

    @NSManaged var deviceTitle: String
    @NSManaged var companyId: String

    @NSManaged var ownerU: PFUser?

    @NSManaged var lastTimeTaken: NSDate?
    @NSManaged var temporaryUser: PFUser?

    @NSManaged var projectInfo: String
    @NSManaged var dueDate: NSDate

    @NSManaged var releaseDate: NSDate

//Mark:-DeviceEntity Bridge

    func convertToDeviceEntity() -> DeviceEntity {
        let convertedOwner = DataStorage.sharedInstance.localUserMappingById(ownerU?.objectId ?? "")
        let convertedTemporaryUser = DataStorage.sharedInstance.localUserMappingById(temporaryUser?.objectId ?? "")

        var deviceOnProject: DeviceProjectInfoEntity?
        deviceOnProject = DeviceProjectInfoEntity(dueDate: dueDate, releaseDate: releaseDate, projectName: projectInfo)

        let device = DeviceEntity(deviceTitle: deviceTitle, companyId: companyId, owner: convertedOwner, currentUser: convertedTemporaryUser, projectInfo: deviceOnProject)
        device.uniqueId = objectId
        device.lastTimeTaken = lastTimeTaken

        return device
    }

}