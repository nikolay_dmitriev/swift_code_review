//
// Created by mac-184 on 1/28/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class ParseToLocalConverter {

//Mark:-Properties

    var parseUsers: [PFUser]?
    var parseDevices: [DeviceParseEntity]?
    var parseRequests: [DeviceRequestParseEntity]?

//Mark:-Private Access to DataStorage

    var addUserEntityToDataStorage: ((newUser:UserEntity) -> ())?
    var addDeviceToDataStorage: ((newDevice:DeviceEntity) -> ())?
    
    //Because for now requests are the only thing that can be deleted, refresh them completely instead of updating
    var replaceRequestsInDataStorage: ((newRequests:[DeviceRequestEntity]) -> ())?

//Mark:-Public

    func convertUsers() {
        for parseUserIterator in parseUsers! {
            let localUser = DataStorage.sharedInstance.localUserMappingById(parseUserIterator.objectId ?? "")
            if localUser == nil {
                let newLocalUser = parseUserIterator.convertToUserEntity()

                //Users can't be deleted
                if parseUserIterator.photo != nil {
                    parseUserIterator.photo?.getDataInBackgroundWithBlock({
                        (receivedData, receivedError) -> Void in
                        if let error = receivedError {
                            print(error)
                        }
                        if let data = receivedData {
                            newLocalUser.photo = UIImage(data: data)
                        }
                    })
                }
                addUserEntityToDataStorage?(newUser: newLocalUser)
                continue
            }
            //For current project state user can't be changed after creation. Will be implemented later
        }
    }

    func convertDevices() {
        for parseDeviceIterator in parseDevices! {
            let localDevice = DataStorage.sharedInstance.localDeviceMappingById(parseDeviceIterator.objectId!)
            if localDevice == nil {
                addDeviceToDataStorage?(newDevice: parseDeviceIterator.convertToDeviceEntity())
                continue
            }
            localDevice!.owner = DataStorage.sharedInstance.localUserMappingById(parseDeviceIterator.ownerU?.objectId ?? "")
            localDevice!.temporaryUser = DataStorage.sharedInstance.localUserMappingById(parseDeviceIterator.temporaryUser?.objectId ?? "")
        }
    }

    func convertRequests() {
        guard let convertedRequests = (parseRequests?.map() {
            $0.convertToLocalRequest()
        }) else{
            return
        }
        replaceRequestsInDataStorage?(newRequests: convertedRequests)
    }
}