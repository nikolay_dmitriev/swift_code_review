//
// Created by mac-184 on 1/26/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class DeleteDeviceRequestFromParseTask: NSOperation {

//Mark:-Properties

    var deviceRequest: DeviceRequestEntity?

//Mark:-Computed Properties

    override var executing: Bool {
        get {
            return _executing
        }
        set {
            willChangeValueForKey("isExecuting")
            _executing = newValue
            didChangeValueForKey("isExecuting")
        }
    }
    private var _executing: Bool = false

    override var finished: Bool {
        get {
            return _finished
        }
        set {
            willChangeValueForKey("isFinished")
            _finished = newValue
            didChangeValueForKey("isFinished")
        }
    }
    private var _finished: Bool = false

//Mark:-Public

    override func start() {
        if cancelled {
            finished = true
            return
        }
        executing = true
        downloadDeviceRequestFromParse()
    }

    override func main() {
        if cancelled {
            finished = true
            return
        }
    }

    override var asynchronous: Bool {
        return true
    }

//Mark:-Private

    private func downloadDeviceRequestFromParse() {
        let query = PFQuery(className: DeviceRequestParseEntity.parseClassName())
        let task = query.getObjectInBackgroundWithId(deviceRequest?.uniqueId ?? "")

        while !task.completed {
        }
        
        deleteDeviceRequest(task.result as? PFObject, error: task.error)
    }

    private func deleteDeviceRequest(deviceRequest: PFObject?, error: NSError?) {
        guard error == nil else{
            print(error)
            completeOperation()
            return
        }

        guard let receivedDeviceRequest = deviceRequest as? DeviceRequestParseEntity else{
            completeOperation()
            return
        }

        receivedDeviceRequest.deleteInBackground()
        completeOperation()
    }

    private func completeOperation(){
        self.finished = true
        self.executing = false
    }

}

