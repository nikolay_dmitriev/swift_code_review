//
// Created by mac-184 on 1/28/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

typealias fetchChangesCallback = ((parseUsers:[PFUser]?, parseDevices:[DeviceParseEntity]?, deviceRequests:[DeviceRequestParseEntity]?) -> ())

class FetchChangesFromParseTask: NSOperation {

//Mark:-Properties

    private var usersFromParse: [PFUser]?
    private var devicesFromParse: [DeviceParseEntity]?
    private var requestsFromParse: [DeviceRequestParseEntity]?

    var lastUpdateDate: NSDate?

    var onFetchComplete: fetchChangesCallback?

//Mark:-Computed Properties

    override var executing: Bool {
        get {
            return _executing
        }
        set {
            willChangeValueForKey("isExecuting")
            _executing = newValue
            didChangeValueForKey("isExecuting")
        }
    }
    private var _executing: Bool = false

    override var finished: Bool {
        get {
            return _finished
        }
        set {
            willChangeValueForKey("isFinished")
            _finished = newValue
            didChangeValueForKey("isFinished")
        }
    }
    private var _finished: Bool = false

//Mark:-Public

    override func start() {
        if cancelled {
            finished = true
            return
        }
        executing = true

        fetchUsersFromParse()
        fetchDevicesFromParse()
        fetchDeviceRequestsFromParse()

        dispatch_async(dispatch_get_main_queue(), {
            self.onFetchComplete?(parseUsers: self.usersFromParse, parseDevices: self.devicesFromParse, deviceRequests: self.requestsFromParse)
        })

        completeOperation()
    }

    override func main() {
        if cancelled {
            finished = true
            return
        }
    }

    override var asynchronous: Bool {
        return true
    }

//Mark:-Private

    private func fetchUsersFromParse() {
        let query = PFUser.query()!
        if let updateWithDate = lastUpdateDate {
            query.whereKey("updatedAt", greaterThan: updateWithDate)
        }
        let task = query.findObjectsInBackground()

        while !task.completed {
        }

        usersFromParse = task.result as? [PFUser]
    }

    private func fetchDevicesFromParse() {
        let query = PFQuery(className: DeviceParseEntity.parseClassName())
        if let updateWithDate = lastUpdateDate {
            query.whereKey("updatedAt", greaterThan: updateWithDate)
        }

        let task = query.findObjectsInBackground()

        while !task.completed {
        }

        devicesFromParse = task.result as? [DeviceParseEntity]
    }

    private func fetchDeviceRequestsFromParse() {
        guard let currentUser = PFUser.currentUser() else{
            completeOperation()
            return
        }

        let currentUserRequests = PFQuery(className: DeviceRequestParseEntity.parseClassName())
        currentUserRequests.whereKey("sender", equalTo: currentUser)

        let innerQueryForDevices = PFQuery(className: DeviceParseEntity.parseClassName())
        innerQueryForDevices.whereKey("ownerU", equalTo: currentUser)

        let ownerShouldReceiveRequests = PFQuery(className: DeviceRequestParseEntity.parseClassName())
        ownerShouldReceiveRequests.whereKey("device", matchesQuery: innerQueryForDevices)
        ownerShouldReceiveRequests.whereKey("currentStatus", equalTo: DeviceRequestStatus.Pending.rawValue)

        let query = PFQuery.orQueryWithSubqueries([currentUserRequests, ownerShouldReceiveRequests])

        let task = query.findObjectsInBackground()


        while task.completed {
        }

        requestsFromParse = task.result as? [DeviceRequestParseEntity]
    }

    private func completeOperation() {
        self.finished = true
        self.executing = false
    }

}
