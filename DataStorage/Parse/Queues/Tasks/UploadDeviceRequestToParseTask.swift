//
// Created by mac-184 on 1/25/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class UploadDeviceRequestToParseTask: NSOperation {

//Mark:-Properties

    var deviceRequest: DeviceRequestEntity?

    private var receivedDevice: DeviceParseEntity?

//Mark:-Computed Properties

    override var executing: Bool {
        get {
            return _executing
        }
        set {
            willChangeValueForKey("isExecuting")
            _executing = newValue
            didChangeValueForKey("isExecuting")
        }
    }
    private var _executing: Bool = false

    override var finished: Bool {
        get {
            return _finished
        }
        set {
            willChangeValueForKey("isFinished")
            _finished = newValue
            didChangeValueForKey("isFinished")
        }
    }
    private var _finished: Bool = false

// MARK:-Public

    override func start() {
        if cancelled {
            finished = true
            return
        }
        
        executing = true
        downloadDeviceFromParse()
    }

    override func main() {
        if cancelled {
            finished = true
            return
        }
    }

    override var asynchronous: Bool {
        return true
    }

//Mark:-Private

    func downloadDeviceFromParse() {
        let query = PFQuery(className: DeviceParseEntity.parseClassName())
        let task = query.getObjectInBackgroundWithId(deviceRequest?.device.uniqueId ?? "")
        while !task.completed {
        }
        onReceiveDevice(task.result as? PFObject, error: task.error)
    }

    func onReceiveDevice(device: PFObject?, error: NSError?) {
        guard error == nil else{
            print(error)
            return
        }
        receivedDevice = device as? DeviceParseEntity
        uploadRequestToParse()
    }

    func uploadRequestToParse() {
        let requestParseEntity = DeviceRequestParseEntity()

        requestParseEntity.sender = PFUser.currentUser()
        requestParseEntity.device = receivedDevice

        requestParseEntity.currentStatus = deviceRequest?.status.rawValue ?? 1

        requestParseEntity.saveInBackground()
        completeOperation()
    }

    private func completeOperation() {
        self.finished = true
        self.executing = false
    }
}