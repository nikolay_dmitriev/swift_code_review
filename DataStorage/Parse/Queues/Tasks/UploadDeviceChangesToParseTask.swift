//
// Created by mac-184 on 1/26/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import Parse

class UploadDeviceChangesToParseTask: NSOperation {

//Mark:-Properties

    var device: DeviceEntity?

//Mark:-Computed Properties

    override var executing: Bool {
        get {
            return _executing
        }
        set {
            willChangeValueForKey("isExecuting")
            _executing = newValue
            didChangeValueForKey("isExecuting")
        }
    }
    private var _executing: Bool = false

    override var finished: Bool {
        get {
            return _finished
        }
        set {
            willChangeValueForKey("isFinished")
            _finished = newValue
            didChangeValueForKey("isFinished")
        }
    }
    private var _finished: Bool = false

//Mark:-Public

    override func start() {
        if cancelled {
            finished = true
            return
        }
        executing = true
        downloadDeviceFromParse()
    }

    override func main() {
        if cancelled {
            finished = true
            return
        }
    }

    override var asynchronous: Bool {
        return true
    }

//Mark:-Private

    private func downloadDeviceFromParse() {
        let query = PFQuery(className: DeviceParseEntity.parseClassName())

        let task = query.getObjectInBackgroundWithId(device?.uniqueId ?? "")
        while !task.completed {
        }

        onReceiveDevice(task.result as? PFObject, error: task.error)
    }

    private func onReceiveDevice(parseDeviceEntity: PFObject?, error: NSError?) {
        guard error == nil  else{
            completeOperation()
            print(error)
            return
        }

        guard let receivedDevice = parseDeviceEntity as? DeviceParseEntity else{
            completeOperation()
            return
        }
        //Temporary user can be changed to every employer except owner, so, need to download new temporary user
        var temporaryOwner: PFUser?
        if device?.temporaryUser != nil {
            let query = PFUser.query()!
            let ownerReceiveTask = query.getObjectInBackgroundWithId(device?.temporaryUser?.uniqueId ?? "")
            while !ownerReceiveTask.completed {
            }
            temporaryOwner = ownerReceiveTask.result as? PFUser
        }

        //We can't change device owner due our application, only reset ownership
        receivedDevice.ownerU = device?.owner == nil ? nil : PFUser.currentUser()
        receivedDevice.temporaryUser = temporaryOwner
        receivedDevice.lastTimeTaken = device?.lastTimeTaken

        receivedDevice.saveInBackground()
        completeOperation()
    }

    private func completeOperation() {
        self.finished = true
        self.executing = false
    }

}