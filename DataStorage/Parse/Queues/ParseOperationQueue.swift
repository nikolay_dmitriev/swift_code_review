//
// Created by mac-184 on 1/25/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

class ParseOperationQueue {

//Mark:-Properties

    private lazy var operationQueue: NSOperationQueue = {
        let queue = NSOperationQueue()
        queue.name = "ParseOperationQueue"
        return queue
    }()

    private lazy var backgrounFetchingQueue: ParseBackgroundFetchingQueue = {
        return ParseBackgroundFetchingQueue()
    }()

//Mark:-Public

    func uploadDeviceRequestToParse(request: DeviceRequestEntity) {
        let uploadTask = UploadDeviceRequestToParseTask()
        uploadTask.deviceRequest = request
        operationQueue.addOperation(uploadTask)
    }

    func uploadDeviceRequestChangesToParse(request: DeviceRequestEntity) {
        let uploadTask = UploadDeviceRequestChangesToParseTask()
        uploadTask.deviceRequest = request
        operationQueue.addOperation(uploadTask)
    }

    func deleteDeviceRequestFromParse(request: DeviceRequestEntity) {
        let deleteTask = DeleteDeviceRequestFromParseTask()
        deleteTask.deviceRequest = request
        operationQueue.addOperation(deleteTask)
    }

    func uploadDeviceChangesToParse(device: DeviceEntity) {
        let uploadChangesTask = UploadDeviceChangesToParseTask()
        uploadChangesTask.device = device
        operationQueue.addOperation(uploadChangesTask)
    }

    func beginFetchingFromParse(onFetchComplete: fetchChangesCallback?){
        backgrounFetchingQueue.onDataReceiveCallback = onFetchComplete
        backgrounFetchingQueue.startFetchingData()
    }

    func stopFetching(){
        backgrounFetchingQueue.stopUpdating()
    }

}