//
// Created by mac-184 on 1/28/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation

class ParseBackgroundFetchingQueue: NSObject {

//Mark:-Properties

    private lazy var backgroundFetchingQueue: NSOperationQueue = {
        let queue = NSOperationQueue()
        queue.name = "ParseBackgroundFetchingQueue"
        return queue
    }()

    private var lastUpdateDate: NSDate?
    private var timer: NSTimer?

    var onDataReceiveCallback: fetchChangesCallback?

//Mark:-Public

    func startFetchingData() {
        timer = NSTimer.scheduledTimerWithTimeInterval(30, target: self, selector: Selector("updateTimerTick"), userInfo: nil, repeats: true)
        launchFetchTask()
    }

    func stopUpdating() {
        timer?.invalidate()
        timer = nil
    }

    func updateTimerTick() {
        if backgroundFetchingQueue.operationCount > 0 {
            return
        }
        launchFetchTask()
    }

    func launchFetchTask() {
        let fetchTask = FetchChangesFromParseTask()
        fetchTask.lastUpdateDate = lastUpdateDate
        fetchTask.onFetchComplete = onDataReceiveCallback
        backgroundFetchingQueue.addOperation(fetchTask)

        lastUpdateDate = NSDate()
    }
}
